extends Control

var MONEY = 0.0
var ITEMS = {
	"TABURE": {
		"NAME": "Tabure",
		"DESCRIPTION": "Günde sattığın çay miktarını arttırır",
		"ICON": preload("res://assets/icons/chair-school.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 0.55,
		"COST": 15,
		"REQUIREMENTS": funcref(self, "item_requirement_tabure"),
		"UPGRADES": [ "ELITE_TABURE" ],
	},
	"CHAIR": {
		"NAME": "Sandalye",
		"DESCRIPTION": "1 adet çay satar",
		"ICON": preload("res://assets/icons/chair-school.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 1,
		"COST": 45,
		"REQUIREMENTS": funcref(self, "item_requirement_chair"),
		"UPGRADES": [],
	},
	"TABLE": {
		"NAME": "Masa",
		"DESCRIPTION": "3 adet çay satar",
		"ICON": preload("res://assets/icons/table-chair.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 3,
		"COST": 300,
		"REQUIREMENTS": funcref(self, "item_requirement_table"),
		"UPGRADES": [ "TABLE_COUSHIN" ],
	},
	"CAYCI": {
		"NAME": "Çaycı",
		"DESCRIPTION": "7 adet çay satar",
		"ICON": preload("res://assets/icons/account.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 7,
		"COST": 1000,
		"REQUIREMENTS": funcref(self, "item_requirement_cayci"),
		"UPGRADES": [ "SPOR_AYAKKABI", "TAKIM_ELBISE" ],
	},
	"SEYYAR_TEZGAH": {
		"NAME": "Seyyar Tezgah",
		"DESCRIPTION": "30 adet çay satar",
		"ICON": preload("res://assets/icons/table-chair.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 30,
		"COST": 5000,
		"REQUIREMENTS": funcref(self, "item_requirement_seyyar_tezgah"),
		"UPGRADES": [],
	},
	"SMALL_SHOP": {
		"NAME": "Küçük Dükkan",
		"DESCRIPTION": "100 adet çay satar",
		"ICON": preload("res://assets/icons/store.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 100,
		"COST": 30000,
		"REQUIREMENTS": funcref(self, "item_requirement_small_shop"),
		"UPGRADES": [ "SMALL_SHOP_KURSU" ],
	},
	"MEDIUM_SHOP": {
		"NAME": "Kıraathane",
		"DESCRIPTION": "300 adet çay satar",
		"ICON": preload("res://assets/icons/store.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 300,
		"COST": 100000,
		"REQUIREMENTS": funcref(self, "item_requirement_medium_shop"),
		"UPGRADES": [ "TELEVISION" ],
	},
	"BIG_SHOP": {
		"NAME": "AVM'de Dükkan",
		"DESCRIPTION": "750 adet çay satar",
		"ICON": preload("res://assets/icons/store.png"),
		"COUNT": 0,
		"CAY_PER_SECOND": 750,
		"COST": 500000,
		"REQUIREMENTS": funcref(self, "item_requirement_big_shop"),
		"UPGRADES": [ "AVM_TERAS" ],
	}
}
var UPGRADES = {
	"ELITE_TABURE": {
		"NAME": "Elit Tabure",
		"DESCRIPTION": "Taburelere elit kesmin gelmesini sağlar ve çay satışını arttırır.",
		"ICON": preload("res://assets/icons/chair-school.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.82 ],
		"COST": [ 50 ],
		"REQUIREMENTS": funcref(self, "upgrade_requirement_elite_tabure"),
	},
	"TABLE_COUSHIN": {
		"NAME": "Masa Örtüsü",
		"DESCRIPTION": "Serdiğin örtü müşterilerin memnuniyetini sağlar.",
		"ICON": preload("res://assets/icons/chair-school.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.55 ],
		"COST": [ 1500 ],
		"REQUIREMENTS": funcref(self, "item_requirement_table"),
	},
	"SPOR_AYAKKABI": {
		"NAME": "Spor Ayakkabı",
		"DESCRIPTION": "Çaycılara vereceğin spor ayakkabı hızlarını artırır.",
		"ICON": preload("res://assets/icons/chair-school.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.42 ],
		"COST": [ 5000 ],
		"REQUIREMENTS": funcref(self, "item_requirement_cayci"),
	},
	"TAKIM_ELBISE": {
		"NAME": "Takım Elbise",
		"DESCRIPTION": "Çaycıların jilet gibi giyinir ve satışları artar.",
		"ICON": preload("res://assets/icons/google-street-view.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.5 ],
		"COST": [ 10000 ],
		"REQUIREMENTS": funcref(self, "item_requirement_cayci"),
	},
	"SMALL_SHOP_KURSU": {
		"NAME": "Kaldırımda Masalar",
		"DESCRIPTION": "Küçük dükkanının önüne masa atarak satışları arttır.",
		"ICON": preload("res://assets/icons/store.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.25, 1.5 ],
		"COST": [ 200000, 350000 ],
		"REQUIREMENTS": funcref(self, "item_requirement_small_shop"),
	},
	"TELEVISION": {
		"NAME": "Digitürk",
		"DESCRIPTION": "Kıraathanelerde maç yayını geliri arttırır.",
		"ICON": preload("res://assets/icons/television.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.16, ],
		"COST": [ 400000 ],
		"REQUIREMENTS": funcref(self, "item_requirement_medium_shop"),
	},
	"AVM_TERAS": {
		"NAME": "AVM'de Teras",
		"DESCRIPTION": "AVM'de ki dükkanlarda teras katında ekstra satış sağlar.",
		"ICON": preload("res://assets/icons/store.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.4 ],
		"COST": [ 3500000 ],
		"REQUIREMENTS": funcref(self, "item_requirement_big_shop"),
	},
	# 1500 TL gelirde cay fiyatina zam yapacaz
	"CAY_ZAM": {
		"NAME": "Çay Zammı",
		"DESCRIPTION": "Çay fiyatını %25 arttırır",
		"ICON": preload("res://assets/icons/cup.png"),
		"LEVEL": -1,
		"LEVEL_MODIFIERS": [ 1.25, 1.25 * 1.20, 1.5 * 1.1666, 1.75 * 1.14286 ],
		"COST": [ 50000, 500000, 2500000, 5000000 ],
		"REQUIREMENTS": funcref(self, "upgrade_requirement_cay_zam"),
	}
}
var CAY_FIYATI_UPGRADES = [
	"CAY_ZAM",
]

var FRAME = 0



func _ready():
	load_game()
	ui_add_buttons()
	ui_set_money()

func _process(delta):
	MONEY += money_per_second() * delta
	ui_update()

func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://profile.save"):
		return
	save_game.open("user://profile.save", File.READ)
	var line = save_game.get_line()
	var profile = parse_json(line)
	MONEY = profile['MONEY']
	for item_id in profile['ITEMS'].keys():
		ITEMS[item_id]['COUNT'] = profile['ITEMS'][item_id]['COUNT']
	for upgrade_id in profile['UPGRADES'].keys():
		UPGRADES[upgrade_id]['LEVEL'] = profile['UPGRADES'][upgrade_id]['LEVEL']

func _notification(event):
	if event == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST or event == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		save_game()

func save_game():
	var save_data = {
		"MONEY": MONEY,
		"ITEMS": {},
		"UPGRADES": {},
	}
	for item_id in ITEMS:
		var item = ITEMS[item_id]
		save_data['ITEMS'][item_id] = {
			"COUNT": item.COUNT,
		}
	for upgrade_id in UPGRADES:
		var upgrade = UPGRADES[upgrade_id]
		save_data['UPGRADES'][upgrade_id] = {
			"LEVEL": upgrade['LEVEL']
		}
	var save_file = File.new()
	save_file.open("user://profile.save", File.WRITE)
	save_file.store_line(to_json(save_data))

func money_per_second():
	var money_per_second = 0.0
	for item_id in ITEMS:
		var item = ITEMS[item_id]
		var cay_per_second = item.CAY_PER_SECOND
		for upgrade_id in item.UPGRADES:
			if UPGRADES[upgrade_id].LEVEL >= 0:
				cay_per_second *= UPGRADES[upgrade_id]['LEVEL_MODIFIERS'][UPGRADES[upgrade_id].LEVEL]
		money_per_second += cay_per_second * item.COUNT * cay_fiyati()
	return money_per_second

func cay_fiyati():
	var base_price = 1.0
	for upgrade_id in CAY_FIYATI_UPGRADES:
		if UPGRADES[upgrade_id].LEVEL >= 0:
			base_price *= UPGRADES[upgrade_id]['LEVEL_MODIFIERS'][UPGRADES[upgrade_id].LEVEL]
	return base_price


func _on_cay_button_pressed():
	MONEY += cay_fiyati()
	ui_set_money()


func ui_update():
	ui_set_money()

	var ITEMS_CONTAINER = get_node("TabContainer/Eşyalar/VBoxContainer")
	for button in ITEMS_CONTAINER.get_children():
		button.set_visible(ITEMS[button.ID].REQUIREMENTS.call_func())
	
	var UPGRADE_CONTAINER = get_node("TabContainer/İyileştirmeler/VBoxContainer")
	for button in UPGRADE_CONTAINER.get_children():
		var visibility = UPGRADES[button.ID].REQUIREMENTS.call_func()

		# Check if upgrade reached max level
		if visibility:
			if UPGRADES[button.ID]['LEVEL'] >= UPGRADES[button.ID]['LEVEL_MODIFIERS'].size() - 1:
				visibility = false
			else:
				button.COUNT = big_num_to_str(UPGRADES[button.ID]['COST'][UPGRADES[button.ID]['LEVEL'] + 1]) + " TL"
				button.TITLE = UPGRADES[button.ID]["NAME"] + " " + str(UPGRADES[button.ID]['LEVEL'] + 2)

		button.set_visible(visibility)
 

func ui_set_money():
	get_node("Hud").MONEY = big_num_to_str(MONEY) + " TL"
	get_node("Hud").CAY_PER_SECOND = "%.2f" % cay_fiyati() + " TL"
	get_node("Hud").MONEY_PER_SECOND = big_num_to_str(money_per_second()) + " TL/gün"
	FRAME = 0

func ui_add_buttons():
	var UPGRADE_BUTTON = preload("res://scenes/UpgradeButton.tscn")
	var ITEMS_CONTAINER = get_node("TabContainer/Eşyalar/VBoxContainer")
	var UPGRADE_CONTAINER = get_node("TabContainer/İyileştirmeler/VBoxContainer")

	# ITEMS dugmelerini ekle
	var ITEM_IDS = ITEMS.keys()
	ITEM_IDS.invert()
	for item_id in ITEM_IDS:
		var item = ITEMS[item_id]
		var button = UPGRADE_BUTTON.instance()
		button.ID = item_id
		button.TITLE = item.NAME
		button.DESCRIPTION = big_num_to_str(item.COST) + " TL" + " - " + item.DESCRIPTION
		button.COUNT = big_num_to_str(item.COUNT)
		button.ICON = item.ICON
		button.connect("pressed", self, "_on_item_button_pressed", [ button ])
		button.set_visible(item.REQUIREMENTS.call_func())
		ITEMS_CONTAINER.add_child(button)

	# UPGRADE dugmelerini ekle
	var UPGRADE_IDS = UPGRADES.keys()
	UPGRADE_IDS.invert()
	for upgrade_id in UPGRADE_IDS:
		var upgrade = UPGRADES[upgrade_id]
		var button = UPGRADE_BUTTON.instance()
		button.ID = upgrade_id
		button.TITLE = upgrade.NAME + " 1"
		button.DESCRIPTION = upgrade.DESCRIPTION
		button.COUNT = big_num_to_str(upgrade.COST[0]) + " TL"
		button.ICON = upgrade.ICON
		button.set_visible(upgrade.REQUIREMENTS.call_func())
		button.connect("pressed", self, "_on_upgrade_button_pressed", [ button ])
		UPGRADE_CONTAINER.add_child(button)

func _on_item_button_pressed(button):
	var item = ITEMS[button.ID]
	if MONEY < item.COST:
		return

	MONEY -= item.COST
	item.COUNT += 1

	button.COUNT = big_num_to_str(item.COUNT)
	get_node("CayButton").coin_rain()


func _on_upgrade_button_pressed(button):
	var upgrade = UPGRADES[button.ID]
	var current_level = upgrade.LEVEL

	# If we are already maxed out upgrade do nothing
	if current_level >= upgrade['LEVEL_MODIFIERS'].size() - 1:
		return

	var next_upgrade_cost = upgrade['COST'][current_level + 1]
	if MONEY < next_upgrade_cost:
		return

	MONEY -= next_upgrade_cost
	upgrade.LEVEL += 1
	get_node("CayButton").coin_rain(50)



# ITEM REQUIREMENTS
func item_requirement_tabure():
	return true

func item_requirement_chair():
	return ITEMS['TABURE']['COUNT'] >= 5

func item_requirement_table():
	return ITEMS['CHAIR']['COUNT'] >= 5

func item_requirement_cayci():
	return ITEMS['TABLE']['COUNT'] >= 10

func item_requirement_seyyar_tezgah():
	return ITEMS['CAYCI']['COUNT'] >= 10

func item_requirement_small_shop():
	return ITEMS['SEYYAR_TEZGAH']['COUNT'] >= 10

func item_requirement_medium_shop():
	return ITEMS['SMALL_SHOP']['COUNT'] >= 10

func item_requirement_big_shop():
	return ITEMS['MEDIUM_SHOP']['COUNT'] >= 10

# UPGRADE REQUIREMENTS
func upgrade_requirement_elite_tabure():
	return true

func upgrade_requirement_cay_zam():
	return money_per_second() >= 1000


func big_num_to_str(big_number, prefix=""):
	var n = big_number
	var n_str = String(round(n))
	if n < 1000000000:
		var insert_comma_position = n_str.length() - 3
		while insert_comma_position > 0:
			n_str = n_str.insert(insert_comma_position, ",")
			insert_comma_position -= 3
		return "%s%s" % [ prefix, n_str ]

	var total_digit_count = floor((log(n) / log(10)) + 1)
	var show_n_digits = (fmod(total_digit_count, 3)) + 1
	var first_numbers = n_str.left(show_n_digits)

	var N = (int(total_digit_count) / 3) - 1

	return "%s%s %s" % [ prefix, first_numbers, latin_suffix(N) ]


func latin_suffix(n):

	if n == 1:
		return "million"
	elif n == 2:
		return "billion"
	elif n == 3:
		return "trillion"
	elif n == 4:
		return "quadrillion"
	elif n == 5:
		return "quintillion"
	elif n == 6:
		return "sextillion"
	elif n == 7:
		return "septillion"
	elif n == 8:
		return "octillion"
	elif n == 9:
		return "nonillion"
	elif n == 10:
		return "decillion"


	var suffixes = [
		[ "un", "deci", "centi" ],
		[ "duo", "viginti", "ducenti" ],
		[ "tre", "triginta", "trecenti" ],
		[ "quattuor", "quadraginta", "quadringenti" ],
		[ "quinqua", "quinquaginta", "quingenti" ],
		[ "se", "sexaginta", "sescenti" ],
		[ "septe", "septuaginta", "septingenti" ],
		[ "octo", "octoginta", "octingenti" ],
		[ "nove", "nonaginta", "nongenti" ],
	]

	var digits_str = String(n)
	var digits = []
	for d in digits_str:
		digits.push_front(int(d))

	var tens = 0
	var suffix = ""
	for i in digits:
		if i != 0:
			suffix += suffixes[i-1][tens]
		tens += 1

	suffix += "llion"
	return suffix


func _on_SaveTimer_timeout():
	save_game()
