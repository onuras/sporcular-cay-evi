extends Button

export(String) var ID = ""
export(String) var TITLE = "" setget set_title
export(String) var DESCRIPTION = "" setget set_description
export(String) var COUNT = "" setget set_count
export(String, FILE) var ICON setget set_icon

func set_title(v):
	get_node("HBoxContainer/VBoxContainer/Title").set_text(v)

func set_description(v):
	get_node("HBoxContainer/VBoxContainer/Description").set_text(v)

func set_count(v):
	get_node("HBoxContainer/Count").set_text(v)

func set_icon(v):
	get_node("HBoxContainer/Icon").texture = v