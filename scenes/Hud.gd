extends Control

export(String) var MONEY = "0" setget set_money
export(String) var MONEY_PER_SECOND = "0" setget set_money_per_second
export(String) var CAY_PER_SECOND = "0" setget set_cay_per_second


func set_money(v):
	get_node("VBoxContainer/ColorRect/Label").set_text(v)

func set_money_per_second(v):
	get_node("VBoxContainer/HBoxContainer/MoneyPerSecond/Label").set_text(v)

func set_cay_per_second(v):
	get_node("VBoxContainer/HBoxContainer/CayPerSecond/Label").set_text(v)