extends TextureButton

onready var COIN = preload("res://scenes/Coin.tscn")
onready var COIN_SMALL = preload("res://scenes/CoinFalling.tscn")
var ENDLESS_COIN_RAIN = false


func _pressed():
	var coin = COIN.instance()
	var position = get_viewport().get_mouse_position()
	coin.position = Vector2(position.x, position.y - 70)
	coin.get_node("AnimationPlayer").play("click")
	add_child(coin)

func _on_animation_finished(animation_name, coin):
	if animation_name == "coin_falling" && ENDLESS_COIN_RAIN == true:
		coin.get_node("AnimationPlayer").play("coin_falling")
	else:
		coin.queue_free()

func coin_rain(adet=0):
	randomize()
	_pressed()
	if adet == 0:
		adet = (randi() % 30) + 10
	var width = get_viewport().size.x
	var increment = width / adet
	for i in range(adet - 1):
		var coin = COIN_SMALL.instance()
		coin.position.x = (i + 1) * increment
		add_child(coin)
		coin.get_node("AnimationPlayer").set_speed_scale(rand_range(0.5, 1.5))
		coin.get_node("AnimationPlayer").play("coin_falling")
		coin.get_node("AnimationPlayer").connect("animation_finished", self, "_on_animation_finished", [ coin ])
