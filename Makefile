
all: pack

pack: export
	cd target && zip -r cay-evi-html5.zip cay-evi-html5
	cd target && zip -r cay-evi-linux.zip cay-evi-linux
	cd target && zip -r cay-evi-windows.zip cay-evi-windows

export: mkdir
	godot -v --export 'HTML5' target/cay-evi-html5/index.html
	godot -v --export 'Linux' target/cay-evi-linux/cay_evi.x86_64
	godot -v --export 'Windows' target/cay-evi-windows/cay_evi.exe

mkdir:
	mkdir -p target/cay-evi-linux target/cay-evi-html5 target/cay-evi-windows

clean:
	rm -rf target
